import React, { useState } from 'react';

import Counter from './components/hook/Counter';
import Hello from './components/hook/Hello';
import Rede from './components/hook/Rede';

const App = () => {
  const [showHello, setShowHello] = useState(true);

  return (
    <>
      <button onClick={() => setShowHello(!showHello)}>Toggle</button>
      {showHello && <Hello />}

      <Counter />
      <Rede />
    </>
  );
};

export default App;
