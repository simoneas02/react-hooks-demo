import { useEffect, useState } from 'react';

export const useFetch = url => {
  const [state, setState] = useState({ data: null, loading: true });

  useEffect(() => {
    setState(state => ({ data: state.data, loading: true }));

    const getData = async () => {
      const requestData = await fetch(url);
      const responseData = await requestData.json();

      setState({ data: responseData, loading: false });
    };

    getData();
    // getDataTest();
    console.log('testing');
  }, [url]);

  return state;
};
