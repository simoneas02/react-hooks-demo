import React, { useState, useEffect, useContext } from 'react';
import { ThemeContext } from '../../context/ThemeContext';
import { useFetch } from '../../hooks/useFetch';
// import { useFetch } from '../hooks/useFetch';

const Rede = () => {
  const [{ data, loading }, setState] = useState({
    data: null,
    loading: false,
  });

  const theme = useContext(ThemeContext);

  const redeData = useFetch('https://api.github.com/orgs/rede-cidada');
  const pokeData = useFetch('https://pokeapi.co/api/v2/pokemon/pikachu');

  return (
    <>
      <h1 style={{ background: theme.background, color: theme.foreground }}>
        Rede Cidadã
      </h1>
      <p>{redeData?.loading ? 'Loading...' : redeData?.data?.description}</p>
      <p>{pokeData?.loading ? 'Loading...' : pokeData?.data?.name}</p>

      <p>{loading ? 'Loading...' : data?.description}</p>
    </>
  );
};

export default Rede;
