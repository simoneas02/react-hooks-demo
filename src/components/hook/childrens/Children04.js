import React from 'react';
import Children05 from './Children05';

const Children04 = () => (
  <>
    <h1>Children04</h1>
    <Children05 />
  </>
);

export default Children04;
