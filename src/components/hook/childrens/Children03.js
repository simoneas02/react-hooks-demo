import React from 'react';
import Children04 from './Children04';

const Children03 = () => (
  <>
    <h1>Children03</h1>
    <Children04 />
  </>
);

export default Children03;
