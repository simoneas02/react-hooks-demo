import React from 'react';
import Children02 from './Children02';

const Children01 = () => (
  <>
    <h1>Children01</h1>
    <Children02 />
  </>
);

export default Children01;
