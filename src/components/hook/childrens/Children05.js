import React, { useContext } from 'react';
import { HelloContext } from '../../../context/HelloContext';

const Children05 = () => {
  const name = useContext(HelloContext);
  return <h1>Children05 {name}</h1>;
};

export default Children05;
