import React from 'react';
import Children03 from './Children03';

const Children02 = () => (
  <>
    <h1>Children02</h1>
    <Children03 />
  </>
);

export default Children02;
