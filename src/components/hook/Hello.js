import React, { useEffect, useState } from 'react';
import Children01 from './childrens/Children01';
import { HelloContext } from '../../context/HelloContext';

const Hello = () => {
  useEffect(() => {
    console.log('mount');

    return () => console.log('unmount');
  });

  return (
    <HelloContext.Provider value={'Beyonce'}>
      <h1>Hello</h1>
      <Children01 />
    </HelloContext.Provider>
  );
};

export default Hello;
